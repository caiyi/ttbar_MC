import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':260000,
    'pdf_variations':[260000],
    'alternative_pdfs':[13100,25200],
    'scale_variations':[0.5,1.,2.],
}
from MadGraphControl.MadGraphUtils import *
import os
import subprocess
import math
import shutil

# This JO is for pure signal production

# Build card
if (model == '2HDMa'):
    proc="""import model Pseudoscalar_2HDM_FormFactor
    define p = g u c d s u~ c~ d~ s~ b b~ 
    define j = p
    generate g g > %s > t t~ QED=99 QCD=99"""%('h3' if (mediator == 'A') else 'h2')

if (model == '2HDM'):
    proc = """import model Higgs_Effective_Couplings_FormFact
    define p = g u c d s u~ c~ d~ s~ b b~ 
    define j = p
    generate g g > %s > t t~ QED=99 QCD=99"""%('h1' if (mediator == 'A') else 'h')

process = """
set default_unset_couplings 99
set group_subprocesses Auto
set ignore_six_quark_processes False
set loop_optimized_output True
set low_mem_multicore_nlo_generation False
set loop_color_flows False
set gauge unitary
set complex_mass_scheme False
set max_npoint_for_channel 0
"""+proc+"""
output -f
"""

process_dir = new_process(process)

# Run card
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    beamEnergy = 6500.

settings = {'cut_decays':'F',
            'clusinfo':"False",
            'pdfwgt':'F',
            'ptj':0.0,
            'pta':0.0,
            'ptl':0.0,
            'etaj':-1.0,
            'etaa':-1.0,
            'etal':-1.0,
            'etab':-1.0,
            'drjj':0.0,
            'drll':0.0,
            'draa':0.0,
            'draj':0.0,           
            'drjl':0.0,      
            'dral':0.0,
            'nevents':10000,
            'beamEnergy':beamEnergy,
            'iseed':21,
            'python_seed':-2,
            'dynamical_scale_choice':"3"}

modify_run_card(process_dir = process_dir,runArgs = runArgs,settings = settings)

# Parameters card
'''
params = {}

if (model == '2HDM'):
    MC = 1.42
    MB = 4.7
    MT = 172.5
    MM = 0.10566
    MTA = 1.777
    sba = 1.
    params['MASS']={'4':MC,
                    '5':MB,
                    '6':MT,
                    '13':MM,
                    '15':MTA}
    if (mediator == 'A'):
        params['MASS'].update({'25':'1.000000e+04',
                               '9000006':mass})
        params['DECAY']={'25':'DECAY 25 5.753088e-03',
                         '9000006':'DECAY 9000006 Auto'}
        params['YUKAWA']={'4':1./tanb*MC,
                          '5':tanb*MB,
                          '6':1./tanb*MT,
                          '13':tanb*MM,
                          '15':tanb*MTA}
    if (mediator == 'H'):
        params['MASS'].update({'25':mass,
                               '9000006':'1.000000e+04'})
        params['DECAY']={'9000006':'DECAY 9000006 5.753088e-03',
                         '25':'DECAY 25 Auto'}
        params['YUKAWA']={'4':(math.cos(math.asin(sba))-sba/tanb)*MC,
                          '5':(math.cos(math.asin(sba))+sba*tanb)*MB,
                          '6':(math.cos(math.asin(sba))-sba/tanb)*MT,
                          '13':(math.cos(math.asin(sba))+sba*tanb)*MM,
                          '15':(math.cos(math.asin(sba))+sba*tanb)*MTA}
if (model == '2HDMa'):
    MZ = 91.1876
    MB = 4.7
    MT = 172.5
    MTA = 1.777
    params['FRBLOCK']={'2':tanb}
    params['HIGGS']={'5':sinp}
    params['MASS']={'5':MB,
                    '6':MT,
                    '15':MTA,
                    '25':'1.250000e+02',
                    '35':mass,
                    '36':mass,
                    '37':mass,
                    '52':'1.000000e+01',
                    '55':ma,
                    '251':'7.982466e+01',
                    '250':'9.118760e+01'}
    params['DECAY']={'25':'DECAY 25 Auto',
                     '35':'DECAY 35 Auto',
                     '36':'DECAY 36 Auto',
                     '37':'DECAY 37 Auto',
                     '55':'DECAY 55 Auto',
                     '250':'DECAY 250 Auto',
                     '251':'DECAY 251 Auto'}
    params['YUKAWA']={'5':MB,
                      '6':MT,
                      '15':MTA}

modify_param_card(process_dir = process_dir,params = params)
'''
if(model == '2HDM'): paramToCopy = 'param_card_2HDM.dat'
if(model == '2HDMa'): paramToCopy = 'param_card_2HDMa.dat'
paramDestination = process_dir+'/Cards/param_card.dat'
paramfile        = subprocess.Popen(['get_files','-data',paramToCopy])
paramfile.wait()
shutil.copy(paramToCopy,paramDestination)

# MadSpin card
os.makedirs(process_dir+'/madspin')

madspin_card = process_dir+'/Cards/madspin_card.dat'
mscard = open(madspin_card,'w') 
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
set max_weight_ps_point 500  # number of PS to estimate the maximum for each event
set ms_dir %s/madspin
set BW_cut 15
set seed 21
define j = g u c d s b u~ c~ d~ s~ b~
decay t > w+ b, w+ > all all 
decay t~ > w- b~, w- > all all
launch
"""%(process_dir))
mscard.close()

# Generation of events
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

evgenConfig.description = "test of 2HDM+a"
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.keywords = ["ggAtt", "ttbar", "singlelepton", "dilepton" ]
evgenConfig.nEventsPerJob = 100
evgenConfig.contact  = ["katharina.behr@cern.ch", "yizhou.cai@cern.ch", "hanfei.ye@cern.ch"]

### Shower 
include("Pythia8_i/Pythia8_Base_Fragment.py")
include("Pythia8_i/Pythia8_MadGraph.py")

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include("GeneratorFilters/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
