//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Sep  7 10:07:07 2020 by ROOT version 6.16/00
// from TTree CollectionTree/CollectionTree
// found on file: ../group.phys-gener.madgraph.206769.ggAtt.TXT.mc12_v1.NTUP_TRUTH.root
//////////////////////////////////////////////////////////

#ifndef tagana_h
#define tagana_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>

// Headers needed by this particular selector
#include "TLorentzVector.h"

#include <vector>



class tagana : public TSelector {
public :
   TTreeReader     fReader;  //!the tree reader
   TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain

   // Readers to access the data (delete the ones you do not need).
   TTreeReaderArray<TLorentzVector> ttx = {fReader, "ttx"};
   TTreeReaderValue<Int_t> tag = {fReader, "tag"};
   TTreeReaderArray<float> weight = {fReader, "weight"};
   /*TTreeReaderArray<TLorentzVector> truth_tx_jx = {fReader, "truth.tx_jx"};
   TTreeReaderArray<int> truth_tx_hasElectron = {fReader, "truth.tx_hasElectron"};
   TTreeReaderArray<int> t_hasS = {fReader, "t_hasS"};
   TTreeReaderArray<int> tx_hasElectron = {fReader, "tx_hasElectron"};
   TTreeReaderArray<TLorentzVector> truth_tx = {fReader, "truth.tx"};
   TTreeReaderArray<TLorentzVector> truth_t = {fReader, "truth.t"};
   TTreeReaderArray<int> t_hasB = {fReader, "t_hasB"};
   TTreeReaderArray<int> t_hasD = {fReader, "t_hasD"};
   TTreeReaderArray<TLorentzVector> t_j = {fReader, "t_j"};
   TTreeReaderArray<int> t_hasElectron = {fReader, "t_hasElectron"};
   TTreeReaderArray<TLorentzVector> tx_jx = {fReader, "tx_jx"};
   TTreeReaderArray<int> truth_t_hasElectron = {fReader, "truth.t_hasElectron"};
   TTreeReaderArray<TLorentzVector> tx = {fReader, "tx"};
   TTreeReaderArray<TLorentzVector> truth_tx_W = {fReader, "truth.tx_W"};
   TTreeReaderArray<TLorentzVector> t_b = {fReader, "t_b"};
   TTreeReaderArray<int> tx_hasB = {fReader, "tx_hasB"};
   TTreeReaderArray<int> tx_hasD = {fReader, "tx_hasD"};
   TTreeReaderArray<int> truth_tx_hasB = {fReader, "truth.tx_hasB"};
   TTreeReaderArray<TLorentzVector> t_W = {fReader, "t_W"};
   TTreeReaderArray<TLorentzVector> tx_j = {fReader, "tx_j"};
   TTreeReaderArray<TLorentzVector> truth_t_jx = {fReader, "truth.t_jx"};
   TTreeReaderArray<int> tx_hasS = {fReader, "tx_hasS"};
   TTreeReaderArray<TLorentzVector> tx_W = {fReader, "tx_W"};
   TTreeReaderArray<TLorentzVector> tx_b = {fReader, "tx_b"};
   TTreeReaderArray<int> ev_hasRec = {fReader, "ev_hasRec"};
   TTreeReaderArray<int> tx_hasTau = {fReader, "tx_hasTau"};
   TTreeReaderArray<TLorentzVector> truth_t_W = {fReader, "truth.t_W"};
   TTreeReaderArray<TLorentzVector> truth_tx_b = {fReader, "truth.tx_b"};
   TTreeReaderArray<int> t_hasMuon = {fReader, "t_hasMuon"};
   TTreeReaderArray<int> truth_t_hasTau = {fReader, "truth.t_hasTau"};
   TTreeReaderArray<TLorentzVector> truth_ttx = {fReader, "truth.ttx"};
   TTreeReaderArray<TLorentzVector> truth_tx_j = {fReader, "truth.tx_j"};
   TTreeReaderArray<int> truth_t_hasB = {fReader, "truth.t_hasB"};
   TTreeReaderArray<int> truth_t_hasMuon = {fReader, "truth.t_hasMuon"};
   TTreeReaderArray<int> tx_hasMuon = {fReader, "tx_hasMuon"};
   TTreeReaderArray<TLorentzVector> truth_t_j = {fReader, "truth.t_j"};
   TTreeReaderArray<int> truth_tx_hasMuon = {fReader, "truth.tx_hasMuon"};
   TTreeReaderArray<float> truth_chi2 = {fReader, "truth.chi2"};
   TTreeReaderArray<TLorentzVector> truth_t_b = {fReader, "truth.t_b"};
   TTreeReaderArray<int> truth_tx_hasTau = {fReader, "truth.tx_hasTau"};
   TTreeReaderArray<TLorentzVector> t = {fReader, "t"};
   TTreeReaderArray<int> t_hasTau = {fReader, "t_hasTau"};
   TTreeReaderArray<TLorentzVector> t_jx = {fReader, "t_jx"};*/

   TH1D *hmttbarS = nullptr;
   TH1D *hmttbarSI = nullptr;
   TH1D *Afinal = nullptr;
   TH1D *A = nullptr;
   TH1D *H = nullptr;

   tagana(TTree * /*tree*/ =0) { }
   virtual ~tagana() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(tagana,0);

};

#endif

#ifdef tagana_cxx
void tagana::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   fReader.SetTree(tree);
}

Bool_t tagana::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}


#endif // #ifdef tagana_cxx
