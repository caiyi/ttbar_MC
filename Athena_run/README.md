setupATLAS centOS

asetup 21.6.62,AthGeneration

Gen_tf.py --inputGeneratorFile=../TXT.440095._000001.tar.gz --jobConfig=./ --outputEVNTFile=./tmp.root

asetup 21.2,AthDerivation,latest

Reco_tf.py --inputEVNTFile ./tmp.root --outputDAODFile test.root --reductionConf TRUTH1
