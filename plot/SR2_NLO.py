#!/usr/bin/env python3
import os,re
import ROOT as root
import numpy as np
import atlasplots as aplt
import tarfile
import sys

def search(path,word="NTUP"):
    input = []
    for filename in os.listdir(path):
        fp = os.path.join(path,filename)
        if os.path.isfile(fp) and word in filename:
            input.append(fp)
    return input

aplt.set_atlas_style()
color = [root.kRed,root.kAzure,root.kOrange,root.kBlue,root.kGreen,root.kYellow,root.kGray,root.kBlack]
ah = ["A","H"]
mass = ["400","700"]
hist=[]

for i2 in mass:
    for i1 in ah:
        n_h = len(hist)
        fig, (ax1, ax2) = aplt.ratio_plot(name="fig"+str(n_h), figsize=(800, 800), hspace=0.05)
        for i3 in ["1","3"]:
            name = "13TeV."+i1+i2+".00400_"
            tarpath = search(path=os.path.join('/eos/user/c/caiyi/MC2/2HDM_MS'+i3,name,'LHE'),word='001.tar.gz')
            lhepath = os.path.join('/tmp/cyz',name)
            tar = tarfile.open(tarpath[0])
            tar.extractall(path=lhepath)
            lines = open(os.path.join(lhepath,tar.getnames()[0]),'r').readlines()
            for line in lines:
                if 'Integrated weight (pb)' in line:
                    break
            xs=float(re.findall(r"\d+\.?\d*",line)[0])
            chain = root.TChain("CollectionTree")
            input = search(path=os.path.join('/eos/user/c/caiyi/MC2/2HDM_MS'+i3,name,'DAOD_TRUTH1'))
            for p in input:
                chain.Add(p)
            hist.append(root.TH1D(str(int(n_h/2))+i3,'',70,300,1000))
            n = chain.GetEntries()
            for i in range(0,n):
                chain.GetEntry(i)
                hist[-1].Fill(chain.ttx[0].M(),chain.weight[0])
            total_weight = hist[-1].Integral()
            hist[-1].Scale(139000*xs/total_weight)
            ax1.plot(hist[-1],label = "NLO" if i3=="3" else "LO", linecolor = color[int((int(i3)-1)/2)]+1,labelfmt = "L")

        ratio_hist = hist[n_h].Clone("ratio_hist")
        ratio_hist.Divide(hist[-1])
        ax2.plot(ratio_hist, linecolor=root.kBlack)
        line = root.TLine(ax2.get_xlim()[0], 1, ax2.get_xlim()[1], 1)
        ax2.plot(line)
        ax2.set_ylim(0.9, 1.2)
        ax2.set_xlabel("Pt of leading top [GeV]")
        ax2.set_ylabel("LO / NLO")
        ax1.cd()
        ax1.add_margins(top=0.1)
        if i2 in ["700","900"]:
            ax1.legend(loc=(0.2, 0.6, 0.45, 0.7))
            aplt.atlas_label(text="Work in progress", loc="upper left")
            ax1.text(0.2, 0.85, "#sqrt{s} = 13 TeV, 139 fb^{-1}", size=24, align=13)
            ax1.text(0.2, 0.8, "tan#beta = 0.4, "+i2+"GeV "+i1+" -> t#bar{t}", size=22, align=13)
        else:
            ax1.legend(loc=(0.6, 0.6, 0.85, 0.7))
            aplt.atlas_label(text="Work in progress", loc="upper right")
            ax1.text(0.55, 0.85, "#sqrt{s} = 13 TeV, 139 fb^{-1}", size=24, align=13)
            ax1.text(0.55, 0.8, "tan#beta = 0.4, "+i2+"GeV "+i1+" -> t#bar{t}", size=22, align=13)
        ax1.set_ylabel("Events / 10GeV")
        fig.savefig("ttbar/SignalRequest{}_7.png".format(int(n_h/2)))
