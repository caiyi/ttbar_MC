#!/usr/bin/env python3
import os
import ROOT as root
import numpy as np
import atlasplots as aplt

def search(path,word="NTUP"):
    input = []
    for filename in os.listdir(path):
        fp = os.path.join(path,filename)
        if os.path.isfile(fp) and word in filename:
            input.append(fp)
    return input

aplt.set_atlas_style()
color = [root.kOrange,root.kAzure,root.kRed,root.kBlue,root.kGreen,root.kYellow,root.kGray,root.kBlack]
fig, (ax1, ax2) = aplt.ratio_plot(name="fig1", figsize=(800, 800), hspace=0.05)

hist = []
dict = {
    'B':["/afs/cern.ch/work/c/caiyi/2HDM_SI/Run/13TeV.A500.00403_/DAOD_TRUTH1",401.132031042],
    'S':["/afs/cern.ch/work/c/caiyi/2HDM_SI/Run/13TeV.A500.00400_/DAOD_TRUTH1",17.7250013717],
    'S+I+B':["/afs/cern.ch/work/c/caiyi/2HDM_SI/Run/13TeV.A500.00402_/DAOD_TRUTH1",401.914331103]
}

for f in dict.keys():
    chain = root.TChain("CollectionTree")
    input = search(path=dict[f][0])
    for p in input:
        chain.Add(p)
    n = chain.GetEntries()
    n_h = len(hist)
    hist.append(root.TH1D("{}".format(n_h),'',40,300,700))
    for i in range(0,n):
        chain.GetEntry(i)
        hist[-1].Fill(chain.ttx[0].M(),chain.weight[0])
    hist[-1].Scale(139000*dict[f][1]/hist[-1].Integral())

hist.append(root.THStack("S+B", ""))
hist[0].SetFillColor(color[0]+1)
hist[1].SetFillColor(color[1]+1)
hist[-1].Add(hist[0])
hist[-1].Add(hist[1])
ax1.plot(hist[-1],label = "Signal+Background")
ax1.plot(hist[2],linecolor = color[2]+1,label = 'Total',labelfmt = "L")

line = root.TLine(ax1.get_xlim()[0], 1, ax1.get_xlim()[1], 1)
ax2.plot(line)
ratio_hist1 = hist[2].Clone("ratio_hist")
ratio_hist1.Divide(hist[-1].GetStack().Last())
ax2.plot(ratio_hist1, "P")
ax2.set_ylim(0.8, 1.2)

ax1.add_margins(top=0.1)
ax2.add_margins(top=0.05)
ax2.set_xlabel("M_{t#bar{t}} [GeV]")
ax1.set_ylabel("Events / 10GeV")
ax2.set_ylabel("SM tt~ / S+B")
ax1.cd()

legend = root.TLegend(0.68, 0.65, 1, 0.90)
legend.SetFillColorAlpha(0, 0)
legend.SetTextSize(22)
legend.AddEntry(hist[2], "Total", "EP")
legend.AddEntry(hist[0], "Background", "F")
legend.AddEntry(hist[1], "Signal", "F")
legend.Draw()

fig.savefig("~/eos/ttbar/SM_SB.png")