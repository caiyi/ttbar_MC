#!/usr/bin/env python3
import os,re,sys
import ROOT as root
import numpy as np
import atlasplots as aplt

me = sys.argv[1]

aplt.set_atlas_style()
color = [root.kGray,root.kAzure,root.kGreen,root.kOrange,root.kYellow,root.kViolet]
file = []
hist = []
ratio_hist = []
dict0 = {'Generated':"/afs/cern.ch/work/c/caiyi/public/rw_plot/rw_{}_0.root".format(me)}
dict = {
        'Reweighted from 400S':"/afs/cern.ch/work/c/caiyi/public/rw_plot/rw_{}_3.root".format(me),
        'Reweighted from 500S':"/afs/cern.ch/work/c/caiyi/public/rw_plot/rw_{}_1.root".format(me),
        'Reweighted from 600S':"/afs/cern.ch/work/c/caiyi/public/rw_plot/rw_{}_4.root".format(me),
        'Reweighted from 700S':"/afs/cern.ch/work/c/caiyi/public/rw_plot/rw_{}_5.root".format(me),
        'Reweighted from 800S':"/afs/cern.ch/work/c/caiyi/public/rw_plot/rw_{}_2.root".format(me),
        'Reweighted from 900S':"/afs/cern.ch/work/c/caiyi/public/rw_plot/rw_{}_6.root".format(me),
        }
fig2, (ax21, ax22) = aplt.ratio_plot(name="fig2", figsize=(800, 800), hspace=0.05)

for f0 in dict0.keys():
    file0 = root.TFile.Open(dict0[f0],"READ")
    hist0=file0.Get("500GeV_{}".format(me)).Clone("hist0")
    hist0.Scale(139000/hist0.GetEntries())
    hist0.Sumw2()
    hist0.SetBinErrorOption(root.TH1.EBinErrorOpt.kPoisson)

hist_total = root.TH1D("All the Reweighted","All the Reweighted",60,300,900)
for f in dict.keys():
    fig1, (ax11, ax12) = aplt.ratio_plot(name="fig1", figsize=(800, 800), hspace=0.05)
    err_band = aplt.root_helpers.hist_to_graph(hist0,show_bin_width=True)
    ax11.plot(err_band, "2", fillcolor=root.kRed+1, fillstyle=3256)
    ax11.plot(hist0, label = f0, linecolor = root.kRed+1, labelfmt = "L")
    ax21.plot(err_band, "2", fillcolor=root.kRed+1, fillstyle=3256)
    ax21.plot(hist0, label = f0, linecolor = root.kRed+1, labelfmt = "L")
    err_ratio = aplt.root_helpers.hist_to_graph(hist0,show_bin_width=True,norm=True)
    ax12.plot(err_ratio, "2", fillcolor=root.kRed, fillstyle=3256)
    ax22.plot(err_ratio, "2", fillcolor=root.kRed, fillstyle=3256)
    n_h = len(hist)
    print(f)
    print(dict[f])
    file.append(root.TFile.Open(dict[f],"READ"))

    hist.append(file[-1].Get("500GeV_{}".format(me)).Clone("hist_{}".format(n_h)))
    hist_total.Add(hist[-1])
    hist[-1].Scale(hist0.Integral()/hist[-1].Integral())
    #hist[-1].Scale(139000/hist[-1].GetEntries())
    ax11.plot(hist[-1], label = f, linecolor = color[n_h]+1, labelfmt = "L")

    ratio_hist.append(hist[-1].Clone("ratio_hist_{}".format(n_h)))
    ratio_hist[-1].Divide(hist0)
    ax12.plot(ratio_hist[-1], label = f, linecolor = color[n_h]+1, labelfmt = "L")
#hist_total.Scale(hist0.Integral()/hist_total.Integral())
    hist_total.Scale(139000/hist_total.GetEntries())
    ax21.plot(hist_total, label = "All the Reweighted", linecolor = root.kGray+1, labelfmt = "L")
    ratio_hist_total=hist_total.Clone("ratio_hist_total")
    ratio_hist_total.Divide(hist0)
    ax22.plot(ratio_hist_total, label = "All the Reweighted", linecolor = root.kGray+1, labelfmt = "L")

    ax11.cd()
    aplt.atlas_label(text="Work in progress", loc="upper right")
    ax11.legend(loc=(0.6, 0.35, 0.9, 0.75),textsize=18)
    line11 = root.TLine(ax11.get_xlim()[0], 0, ax11.get_xlim()[1], 0)
    ax11.plot(line11)
    ax11.add_margins(top=0.1)
    ax11.text(0.4, 0.8, "500GeV {} SI, normalizated to generated Xsec".format(me), size=22, align=13)
    ax11.set_ylabel("Events / 10GeV")
    ax12.cd()
    line12 = root.TLine(ax12.get_xlim()[0], 1, ax12.get_xlim()[1], 1)
    ax12.plot(line12, linecolor = root.kRed+1, linewidth = 2)                                  
    ax12.set_ylim(0.8, 1.2)
    ax12.set_xlabel("M_{t#bar{t}} [GeV]")
    ax12.set_ylabel("Ratio")
    fig1.savefig("ttbar/reweight_original0{}_{}.png".format(n_h+1,me))
"""
ax21.cd()
aplt.atlas_label(text="Work in progress", loc="upper right")
ax21.legend(loc=(0.6, 0.65, 0.9, 0.75),textsize=20)
line21 = root.TLine(ax21.get_xlim()[0], 0, ax21.get_xlim()[1], 0)
ax21.plot(line21)
ax21.add_margins(top=0.1)
ax21.text(0.4, 0.8, "500GeV {} SI, normalizated to 139000/n_event".format(me), size=22, align=13)
ax21.text(0.4, 0.85, "from 400,600,700,900", size=22, align=13)
ax21.set_ylabel("Events / 10GeV")
ax22.cd()
line22 = root.TLine(ax22.get_xlim()[0], 1, ax22.get_xlim()[1], 1)
ax22.plot(line22, linecolor = root.kRed+1, linewidth = 2)
ax22.set_ylim(0.8, 1.2)
ax22.set_xlabel("M_{t#bar{t}} [GeV]")
ax22.set_ylabel("Ratio")
fig2.savefig("ttbar/reweight_original2_combine_{}.png".format(me))
"""
