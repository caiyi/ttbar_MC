#!/usr/bin/env python3
import os,re
import ROOT as root
import numpy as np
import atlasplots as aplt
import tarfile
import sys

def search(path,word="NTUP"):
    input = []
    for filename in os.listdir(path):
        fp = os.path.join(path,filename)
        if os.path.isfile(fp) and word in filename:
            input.append(fp)
    return input

aplt.set_atlas_style()
color = [root.kRed,root.kAzure,root.kOrange,root.kBlue,root.kGreen,root.kYellow,root.kGray,root.kBlack]
ah = ["H"]
mass = ["400"]
hist=[]

for i2 in mass:
    for i1 in ah:
        fig, (ax1, ax2) = aplt.ratio_plot(name="fig", figsize=(800, 800), hspace=0.05)
        for i3 in ["2HDM_MS0","2HDM_MS"]:
            name = "13TeV."+i1+i2+".00400_"
            tarpath = search(path=os.path.join('/afs/cern.ch/work/c/caiyi',i3,'Run',name,'LHE'),word='001.tar.gz')
            lhepath = os.path.join('/tmp/cyz',name)
            tar = tarfile.open(tarpath[0])
            tar.extractall(path=lhepath)
            lines = open(os.path.join(lhepath,tar.getnames()[0]),'r').readlines()
            for line in lines:
                if 'Integrated weight (pb)' in line:
                    break
            xs=float(re.findall(r"\d+\.?\d*",line)[0])
            chain = root.TChain("CollectionTree")
            input = search(path=os.path.join('/afs/cern.ch/work/c/caiyi',i3,'Run',name,'DAOD_TRUTH1'))
            for p in input:
                chain.Add(p)
            n_h = len(hist)
            hist.append(root.TH1D("{}".format(n_h),'',30,0,300))
            n = chain.GetEntries()
            for i in range(0,n):
                chain.GetEntry(i)
                hist[-1].Fill(chain.add_pt_sl_j[0],chain.weight[0])
            total_weight = hist[-1].Integral()
            hist[-1].Scale(139000*xs/total_weight)
            ax1.plot(hist[-1],label = "Original" if i3=="2HDM_MS" else "Modified", linecolor = color[n_h]+1,labelfmt = "L")

        ratio_hist = hist[0].Clone("ratio_hist")
        ratio_hist.Divide(hist[1])
        ax2.plot(ratio_hist, linecolor=root.kBlack)
        line = root.TLine(ax2.get_xlim()[0], 1, ax2.get_xlim()[1], 1)
        ax2.plot(line)
        ax2.set_ylim(0.9, 1.1)
        ax2.set_xlabel("Pt of leading-lepton")
        ax2.set_ylabel("Modified / Original")
        ax1.cd()
        ax1.add_margins(top=0.1)
        ax1.legend(loc=(0.6, 0.55, 0.85, 0.7))
        aplt.atlas_label(text="Work in progress", loc="upper right")
        ax1.text(0.57, 0.85, "#sqrt{s} = 13 TeV, 139 fb^{-1}", size=24, align=13)
        ax1.text(0.57, 0.8, "tan#beta = 0.4, "+i2+"GeV "+i1+" -> t#bar{t}", size=22, align=13)
        ax1.set_ylabel("Events / 10GeV")
        fig.savefig("ttbar/SignalVali_leadingleppt.png")
