#!/usr/bin/env python3
import os,re
import ROOT as root
import numpy as np
import atlasplots as aplt
import tarfile
import sys

def search(path,word="NTUP"):
    input = []
    for filename in os.listdir(path):
        fp = os.path.join(path,filename)
        if os.path.isfile(fp) and word in filename:
            input.append(fp)
    return input

aplt.set_atlas_style()
color = [root.kRed,root.kAzure,root.kOrange,root.kBlue,root.kGreen,root.kYellow,root.kGray,root.kBlack]
fig1, ax1 = aplt.subplots(name="fig1", figsize=(800, 600))

ii = int(sys.argv[1])

hist = []
dict = {
    '400 S 2HDM A':["2HDM_MS1/Run/13TeV.A400.00400_",0],
    '400 S 2HDM H':["2HDM_MS1/Run/13TeV.H400.00400_",1],
    '600 S 2HDM A':["2HDM_MS1/Run/13TeV.A600.00400_",2],
    '600 S 2HDM H':["2HDM_MS1/Run/13TeV.H600.00400_",3],
    '700 S 2HDM A':["2HDM_MS1/Run/13TeV.A700.00400_",4],
    '700 S 2HDM H':["2HDM_MS1/Run/13TeV.H700.00400_",5],
}

ax1.cd()
for f in dict.keys():
    if not(dict[f][1] in [ii]):
        continue
    tarpath = search(path=os.path.join('/afs/cern.ch/work/c/caiyi',dict[f][0],'LHE'),word='001.tar.gz')
    lhepath = os.path.join('/tmp/cyz',f)
    tar = tarfile.open(tarpath[0])
    tar.extractall(path=lhepath)
    lines = open(os.path.join(lhepath,tar.getnames()[0]),'r').readlines()
    for line in lines:
        if 'Integrated weight (pb)' in line:
            break
    xs=float(re.findall(r"\d+\.?\d*",line)[0])
    print(xs)
    chain = root.TChain("CollectionTree")
    input = search(path=os.path.join('/afs/cern.ch/work/c/caiyi',dict[f][0],'DAOD_TRUTH1'))
    for p in input:
        chain.Add(p)
    n_h = len(hist)
    hist.append(root.TH1D("{}".format(n_h),'',70,300,1000))
    n = chain.GetEntries()
    for i in range(0,n):
        chain.GetEntry(i)
        n_l=0
        nx_l=0
        hist[-1].Fill(chain.ttx[0].M(),chain.weight[0])
    total_weight = hist[-1].Integral()
    print(total_weight)
    hist[-1].Scale(139000*xs/total_weight)
    ax1.plot(hist[-1],label = f, linecolor = color[n_h]+1,labelfmt = "L")

line1 = root.TLine(ax1.get_xlim()[0], 0, ax1.get_xlim()[1], 0)
ax1.plot(line1)
ax1.legend(loc=(0.65, 0.8, 0.85, 0.95))
ax1.add_margins(top=0.1)
ax1.set_xlabel("M_{t#bar{t}} [GeV]")
ax1.set_ylabel("Events / 10GeV")
fig1.savefig("ttbar/SR{}.png".format(ii))
