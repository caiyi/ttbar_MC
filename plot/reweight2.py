#!/usr/bin/env python3
import os,re
import ROOT as root
import numpy as np
import atlasplots as aplt
import tarfile
import sys

def getkwfile(flist, keyword):
    res = []
    for ff in flist:
        if keyword in ff.split('/')[-1]:
            res.append(ff)
    return res

def getAllSub(path):
    Filelist = []
    for home, dirs, files in os.walk(path):
        for filename in files:
            Filelist.append(os.path.join(home, filename))
    return Filelist

aplt.set_atlas_style()
color = [root.kRed,root.kAzure,root.kBlack,root.kOrange,root.kBlue,root.kGreen,root.kYellow,root.kGray]
hist = []
path1 = '/afs/cern.ch/work/c/caiyi/2HDM/Run/13TeV.A500.00400_100.0000/DAOD_TRUTH1'
path2 = '/afs/cern.ch/work/c/caiyi/2HDM1/Run/13TeV.A500.00400_100.0000/DAOD_TRUTH1'
subList = getAllSub(path1)+getAllSub(path2)
resList = getkwfile(subList,"NTUP_TRUTH.root")
resList = ['/afs/cern.ch/work/c/caiyi/2HDM_reweight/Run/13TeV.A500.00401_100.0000/DAOD_TRUTH1',
           '/afs/cern.ch/work/c/caiyi/2HDM/Run/13TeV.A500.00401_100.0000/DAOD_TRUTH1']
fig, (ax1, ax2) = aplt.ratio_plot(name="fig", figsize=(800, 800), hspace=0.05)

for path in resList:
    list = getAllSub(path)
    inputlist = getkwfile(list,"NTUP_TRUTH")
    chain = root.TChain("CollectionTree")
    for input in inputlist:
        chain.Add(input)
    n_h = len(hist)
        
    hist.append(root.TH1D("{}".format(n_h),'',60,300,900))
    n = chain.GetEntries()
    print(n)
    for i in range(0,n):
        chain.GetEntry(i)
        hist[-1].Fill(chain.ttx[0].M(),chain.weight[0])
    total_weight = hist[-1].Integral()
    hist[-1].Scale(139000/n)

ax1.plot(hist[0],label = "Combine reweighted", linecolor = color[0]+1,labelfmt = "L")
ax1.plot(hist[1],label = "Generated",labelfmt = "L")
hist[0].Sumw2()
hist[1].Sumw2()
hist[0].SetBinErrorOption(root.TH1.EBinErrorOpt.kPoisson)
hist[1].SetBinErrorOption(root.TH1.EBinErrorOpt.kPoisson)
err_band_0 = aplt.root_helpers.hist_to_graph(hist[0],show_bin_width=True)
err_band_1 = aplt.root_helpers.hist_to_graph(hist[1],show_bin_width=True)
ax1.plot(err_band_0, "2", fillcolor=color[0]+1, fillstyle=3256)
ax1.plot(err_band_1, "2", fillcolor=root.kBlack, fillstyle=3252)
line1 = root.TLine(ax1.get_xlim()[0], 0, ax1.get_xlim()[1], 0)
ax1.plot(line1)

ratio_hist = hist[0].Clone("ratio_hist")
ratio_hist.Divide(hist[1])
ax2.plot(ratio_hist, linecolor = color[0]+1,labelfmt = "L")
ratio_hist.Sumw2()
ratio_hist.SetBinErrorOption(root.TH1.EBinErrorOpt.kPoisson)
err_ratio_0 = aplt.root_helpers.hist_to_graph(ratio_hist,show_bin_width=True)
ax2.plot(err_ratio_0, "2", fillcolor=color[0]+1, fillstyle=3256)
err_ratio_1 = aplt.root_helpers.hist_to_graph(hist[1],show_bin_width=True,norm=True)
ax2.plot(err_ratio_1, "2", fillcolor=root.kBlack, fillstyle=3252)
line2 = root.TLine(ax2.get_xlim()[0], 1, ax2.get_xlim()[1], 1)
ax2.plot(line2)

ax2.set_ylim(0.9, 1.1)
ax2.set_xlabel("M(t#bar{t}) [GeV]")
ax2.set_ylabel("Ratio")
ax1.cd()
ax1.add_margins(top=0.1)
ax1.legend(loc=(0.6, 0.35, 0.85, 0.55))
aplt.atlas_label(text="Work in progress", loc="upper right")
ax1.text(0.57, 0.85, "#sqrt{s} = 13 TeV, 139 fb^{-1}", size=24, align=13)
ax1.text(0.57, 0.8, "tan#beta = 0.4, 500GeV A -> t#bar{t}, 2HDMa", size=22, align=13)
ax1.set_ylabel("Events / 10GeV")
fig.savefig("ttbar/reweight/Comp_aSI_tmp.png")
