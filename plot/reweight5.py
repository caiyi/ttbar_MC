#!/usr/bin/env python3
import os,re
import ROOT as root
import numpy as np
import atlasplots as aplt
import tarfile
import sys

def getkwfile(flist, keyword):
    res = []
    for ff in flist:
        if keyword in ff.split('/')[-1]:
            res.append(ff)
    return res

def getAllSub(path):
    Filelist = []
    for home, dirs, files in os.walk(path):
        for filename in files:
            Filelist.append(os.path.join(home, filename))
    return Filelist

aplt.set_atlas_style()
color = [root.kRed,root.kAzure,root.kOrange,root.kBlue,root.kGreen,root.kYellow,root.kGray,root.kBlack]
hist = []
#subList = getAllSub(path1)+getAllSub(path2)
#resList = getkwfile(subList,"NTUP_TRUTH.root")
path = ['/afs/cern.ch/work/c/caiyi/2HDM/Run/13TeV.A500.00404_/DAOD_TRUTH1',
        '/afs/cern.ch/work/c/caiyi/2HDM_reweight/Run/13TeV.A500.00404_/DAOD_TRUTH1']
subList = getAllSub(path[0])+getAllSub(path[1])
resList = getkwfile(subList,"NTUP_TRUTH")
fig1, ax1 = aplt.subplots(name="fig1", figsize=(800, 600))

for input in resList:
    chain = root.TChain("CollectionTree")
    chain.Add(input)
    n_h = len(hist)
        
    hist.append(root.TH1D("{}".format(n_h),'',60,300,900))
    n = chain.GetEntries()
    print(n)
    for i in range(0,n):
        chain.GetEntry(i)
        hist[-1].Fill(chain.ttx[0].M(),chain.weight[0])
    total_weight = hist[-1].Integral()
    hist[-1].Scale(139000/n)

ax1.plot(hist[0],label = "Generated", linecolor = color[0]+1,labelfmt = "L")
ax1.plot(hist[1],label = "Reweight", linecolor = color[1]+1,labelfmt = "L")

ax1.add_margins(top=0.1)
ax1.set_xlabel("M_{t#bar{t}} [GeV]")
ax1.set_ylabel("Events / 10GeV")
line1 = root.TLine(ax1.get_xlim()[0], 0, ax1.get_xlim()[1], 0)
ax1.plot(line1)
ax1.add_margins(top=0.1)
ax1.legend(loc=(0.6, 0.55, 0.85, 0.7))
aplt.atlas_label(text="Work in progress", loc="upper right")
ax1.text(0.57, 0.85, "#sqrt{s} = 13 TeV, 139 fb^{-1}", size=24, align=13)
fig1.savefig("ttbar/reweight/Comp_{}.png".format(resList[0].split('/')[-1].strip(".NTUP_TRUTH.root")))
