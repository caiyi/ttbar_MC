#!/usr/bin/env python3
import ROOT as root
import os,re
import numpy as np
import atlasplots as aplt
import tarfile
import sys

def search(path,word="NTUP"):
    input = []
    for filename in os.listdir(path):
        fp = os.path.join(path,filename)
        if os.path.isfile(fp) and word in filename:
            input.append(fp)
    return input

aplt.set_atlas_style()
color = [root.kRed,root.kAzure,root.kOrange,root.kBlue,root.kGreen,root.kYellow,root.kGray,root.kBlack]
ah = ["A","H"]
mass = ["400","600","700","900"]
hist=[]

for i2 in mass:
    for i1 in ah:
        name = "13TeV."+i1+i2+".00400_"
        tarpath = search(path=os.path.join('/afs/cern.ch/work/c/caiyi/2HDM_MS/Run',name,'LHE'),word='001.tar.gz')
        lhepath = os.path.join('/tmp/cyz',name)
        tar = tarfile.open(tarpath[0])
        tar.extractall(path=lhepath)
        lines = open(os.path.join(lhepath,tar.getnames()[0]),'r').readlines()
        for line in lines:
            if 'Integrated weight (pb)' in line:
                break
        chain = root.TChain("CollectionTree")
        input = search(path=os.path.join('/afs/cern.ch/work/c/caiyi/2HDM_MS/Run',name,'DAOD_TRUTH1'))
        for p in input:
            chain.Add(p)
        n = chain.GetEntries()
        print(n)
        xs=float(re.findall(r"\d+\.?\d*",line)[0])
        data = np.loadtxt(os.path.join('/afs/cern.ch/work/c/caiyi/2HDM_MS/Run',name,'DAOD_TRUTH1','log.txt'))
        n_h = len(hist)
        fig, ax = aplt.subplots(name="fig"+str(n_h), figsize=(800, 600))
        hist.append(root.TH1D("{}".format(n_h),'',80,0,2))
        for i in range(0,data.shape[0]-1):
            hist[-1].Fill(abs(data[i][0]-data[i][1])/np.pi)
        hist[-1].Scale(139000*xs/n)
        ax.plot(hist[-1],label = "Signal", linecolor = color[0]+1,labelfmt = "L")

        line = root.TLine(ax.get_xlim()[0], 0, ax.get_xlim()[1], 0)
        ax.plot(line)
        ax.add_margins(top=0.1)
        ax.legend(loc=(0.65, 0.6, 0.85, 0.7))
        aplt.atlas_label(text="Work in progress", loc="upper right")
        ax.text(0.65, 0.85, "#sqrt{s} = 13 TeV, 139 fb^{-1}", size=24, align=13)
        ax.text(0.62, 0.8, "tan#beta = 0.4, "+i2+"GeV "+i1+" -> t#bar{t}", size=22, align=13)
        ax.set_xlabel("|#Delta#phi| [#pi]")
        ax.set_ylabel("Events")
        fig.savefig("ttbar/SignalRequest{}_0.png".format(n_h))
