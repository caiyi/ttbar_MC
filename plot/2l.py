#!/usr/bin/env python3
import os
import ROOT as root
import numpy as np
import atlasplots as aplt

def search(path,word="NTUP"):
    input = []
    for filename in os.listdir(path):
        fp = os.path.join(path,filename)
        if os.path.isfile(fp) and word in filename:
            input.append(fp)
    return input

aplt.set_atlas_style()
color = [root.kRed,root.kAzure,root.kOrange,root.kBlue,root.kGreen,root.kYellow,root.kGray,root.kBlack]
fig, ax = aplt.subplots(name="fig1", figsize=(800, 600))
fig2, ax2 = aplt.subplots(name="fig2", figsize=(800, 600))

hist = []
dict = {
    'SM t#bar{t}':["/afs/cern.ch/work/c/caiyi/2HDM/Run/13TeV.A10000.00403_/DAOD_TRUTH1",401.132031042],
    #'S':["/afs/cern.ch/work/c/caiyi/2HDM_SI/Run/13TeV.A500.00400_/DAOD_TRUTH1",17.7250013717],
    '(S + I) * 20':["/afs/cern.ch/work/c/caiyi/2HDM_SI/Run/13TeV.A500.00401_/DAOD_TRUTH1",20*0.778070060212]
}

for f in dict.keys():
    chain = root.TChain("CollectionTree")
    input = search(path=dict[f][0])
    for p in input:
        chain.Add(p)
    n = chain.GetEntries()
    n_h = len(hist)
    hh=root.TH1D("t_{}".format(n_h),'',70,300,1000)
    hist.append(root.TH1D("{}_mtt".format(n_h),'',70,300,1000))
    hist.append(root.TH1D("{}_cos".format(n_h),'',20,-1,1))
    for i in range(0,n):
        chain.GetEntry(i)
        n_l=0
        nx_l=0
        hh.Fill(chain.ttx[0].M(),chain.weight[0])
        if chain.t_hasElectron[0]==1:
            n_l=n_l+1
        if chain.tx_hasElectron[0]==1:
            nx_l=nx_l+1
        if chain.t_hasMuon[0]==1:
            n_l=n_l+1
        if chain.tx_hasMuon[0]==1:
            nx_l=nx_l+1
        if n_l+nx_l==2:
            if chain.t_hasElectron[0]==1:
                t_l=chain.t_e[0]
                t_nu=chain.t_enu[0]
            elif chain.t_hasMuon[0]==1:
                t_l=chain.t_mu[0]
                t_nu=chain.t_munu[0]
            if chain.tx_hasElectron[0]==1:
                tx_l=chain.tx_e[0]
                tx_nu=chain.tx_enu[0]
            elif chain.tx_hasMuon[0]==1:
                tx_l=chain.tx_mu[0]
                tx_nu=chain.tx_munu[0]
            if t_l.P()>tx_l.P():
                if tx_l.Perp()<20 or t_l.Perp()<25:
                    continue
            else:
                if t_l.Perp()<20 or tx_l.Perp()<25:
                    continue
            if (chain.t_hasElectron[0]==1 and chain.tx_hasElectron[0]) or (chain.t_hasMuon[0]==1 and chain.tx_hasMuon[0]==1):
                if (t_l+tx_l).M()>76 and (t_l+tx_l).M()<105:
                    continue
                if (t_nu+tx_nu).Perp()<40:
                    continue
            if chain.t_b[0].Perp()<30 or chain.tx_b[0].Perp()<30:
                continue
            if chain.t_b[0].Eta()>2.4 or chain.tx_b[0].Eta()>2.4:
                continue
            t_l.Boost(-chain.ttx[0].BoostVector())
            chain.t[0].Boost(-chain.ttx[0].BoostVector())
            t_l.Boost(-chain.t[0].BoostVector())
            tx_l.Boost(-chain.ttx[0].BoostVector())
            chain.tx[0].Boost(-chain.ttx[0].BoostVector())
            tx_l.Boost(-chain.tx[0].BoostVector())
            hist[n_h].Fill(chain.ttx[0].M(),chain.weight[0])
            hist[n_h+1].Fill(np.cos(t_l.Angle(tx_l.Vect())),chain.weight[0])
                
    hist[n_h].Scale(139000*dict[f][1]/hh.Integral())
    hist[n_h+1].Scale(139000*dict[f][1]/hh.Integral())
    #ax.plot(hist[n_h],label = f, linecolor = color[n_h]+1,labelfmt = "L")
    ax2.plot(hist[n_h+1],label = f, linecolor = color[n_h]+1,labelfmt = "L")

'''
ax.add_margins(top=0.1)
ax.set_xlabel("M_{t#bar{t}} [GeV]")
ax.set_ylabel("Events / 10GeV")
ax.legend(loc=(0.58, 0.78, 0.8, 0.90))
line = root.TLine(ax.get_xlim()[0], 0, ax.get_xlim()[1], 0)
ax.plot(line)
fig.savefig("~/eos/ttbar/2l_mtt.png")
'''
ax2.add_margins(top=0.1)
ax2.set_xlabel("c_{hel}")
ax2.set_ylabel("Events / 0.1")
ax2.legend(loc=(0.58, 0.78, 0.8, 0.90))
line2 = root.TLine(ax2.get_xlim()[0], 0, ax2.get_xlim()[1], 0)
ax2.plot(line2)
fig2.savefig("~/eos/ttbar/2l_chel.png")

print(hist[0].GetEntries())
print(hist[2].GetEntries())