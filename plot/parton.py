#!/usr/bin/env python3
import os,re
import ROOT as root
import numpy as np
import atlasplots as aplt
import tarfile
import sys

aplt.set_atlas_style()
color = [root.kRed,root.kAzure,root.kOrange,root.kBlue,root.kGreen,root.kYellow,root.kGray,root.kBlack]

fig1, ax1 = aplt.subplots(name="fig1", figsize=(800, 600))
fig2, ax2 = aplt.subplots(name="fig2", figsize=(800, 600))
hist = []

data1 = np.loadtxt('dat1')
hist.append(root.TH1D("{}".format(len(hist)),'',70,300,1000))
for i in range(70):
    hist[-1].Fill(305+i*10,data1[i,0])
hist[-1].Scale(139000*40.1950031105/1.028149e+225)
ax1.plot(hist[-1], linecolor = color[0]+1,labelfmt = "L")

ax1.add_margins(top=0.1)
ax1.text(0.5, 0.84, "Parton-level", size=30)
ax1.set_xlabel("M_{t#bar{t}} [GeV]")
ax1.set_ylabel("Events / 10GeV")
fig1.savefig("ttbar/par3.png")

data2 = np.loadtxt('dat2')
hist.append(root.TH1D("{}".format(len(hist)),'',80,0,800))
for i in range(80):
    hist[-1].Fill(5+i*10,data2[i,0])
hist[-1].Scale(139000*40.1950031105/(1.028149e+225))
ax2.plot(hist[-1], linecolor = color[1]+1,labelfmt = "L")

ax2.add_margins(top=0.1)
ax2.text(0.5, 0.84, "Parton-level", size=30)
ax2.set_xlabel("Pt of Leading Top [GeV]")
ax2.set_ylabel("Events / 10GeV")
fig2.savefig("ttbar/par4.png")