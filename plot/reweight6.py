#!/usr/bin/env python
import os,re
import utils
import ROOT as root
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as aplt
import tarfile
import sys

color = [root.kRed,root.kAzure,root.kOrange,root.kBlue,root.kGreen,root.kYellow,root.kGray,root.kBlack]
path = ['/afs/cern.ch/work/c/caiyi/eos/2HDM/Run/13TeV.A500.00400_/EXOT/DAOD_EXOT4.24969161._000001.pool.root.1',
        '/afs/cern.ch/work/c/caiyi/eos/2HDM/Run/13TeV.A500.00400_/EXOT/DAOD_EXOT4.24969161._000002.pool.root.1']
fig1, ax1 = aplt.subplots( figsize=(800, 600))

utils.rcSetup(os.path.join("Core","AnalysisTop"))
chain = root.TChain("CollectionTree")
for f in path:
    chain.Add(f)
tree = root.xAOD.MakeTransientTree(chain)
chain = root.TChain("CollectionTree")
chain.Add("/afs/cern.ch/work/c/caiyi/public/AHtt_NewWeights/506769_A500S0.4_weight.root")
hist=root.TH1D("1",'',60,300,900)

for i in xrange(tree.GetEntriesFast()):
    tree.GetEntry(i)
    chain.GetEntry(i)
    is_t = False
    is_tx = False
    for top in tree.TruthParticles:
        if (top.pdgId() == 6) and not(is_t):
            is_t = True
            t = top.p4()
        elif (top.pdgId() == -6) and not(is_tx):
            is_tx = True
            tx = top.p4()
        if is_t and is_tx:
            break
    hist.Fill((t+tx).M(),getattr(chain,"w_A500SI0.4"))
    
ax1.plot(hist,label = "A500SI0.4", linecolor = color[1]+1,labelfmt = "L")

ax1.add_margins(top=0.1)
ax1.set_xlabel("M_{t#bar{t}} [GeV]")
ax1.set_ylabel("Events / 10GeV")
line1 = root.TLine(ax1.get_xlim()[0], 0, ax1.get_xlim()[1], 0)
ax1.plot(line1)
ax1.add_margins(top=0.1)
ax1.legend(loc=(0.6, 0.55, 0.85, 0.7))
ax1.text(0.57, 0.85, "#sqrt{s} = 13 TeV, 139 fb^{-1}", size=24, align=13)
fig1.savefig("Comp_reweight.png")
