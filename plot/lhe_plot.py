import ROOT as root
import sys
import atlasplots as aplt
aplt.set_atlas_style()

numb = int(sys.argv[1])
titles = ["1000GeV 0.4 SI A","1200GeV 0.4 SI A","1400GeV 0.4 SI A","1000GeV 0.4 SI H","1200GeV 0.4 SI H","1400GeV 0.4 SI H","1000GeV 0.4 S A","1200GeV 0.4 S A","1400GeV 0.4 S A","1000GeV 0.4 S H","1200GeV 0.4 S H","1400GeV 0.4 S H"]
paths = ["13TeV.A1000.00401_","13TeV.A1200.00401_","13TeV.A1400.00401_","13TeV.H1000.00401_","13TeV.H1200.00401_","13TeV.H1400.00401_","13TeV.A1000.00400_","13TeV.A1200.00400_","13TeV.A1400.00400_","13TeV.H1000.00400_","13TeV.H1200.00400_","13TeV.H1400.00400_"]
title = titles[numb]
path = paths[numb]

file = root.TFile.Open("/afs/cern.ch/work/c/caiyi/ttbar_MC/plot/lhe_re/"+path+".root","READ")
hist = file.Get(title)
fig, ax = aplt.subplots(name="fig", figsize=(800, 600))

ax.plot(hist,label = title, linecolor = root.kRed+1,labelfmt = "L")
ax.set_xlabel("M_{t#bar{t}} [GeV]")
ax.set_ylabel("Events / 10GeV")
ax.add_margins(top=0.05,bottom=0.05)
ax.legend(loc=(0.15, 0.8, 0.5, 0.9))
fig.savefig("ttbar/new_req/"+path+".png")