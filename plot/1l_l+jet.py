#!/usr/bin/env python3
import os
import ROOT as root
import numpy as np
import atlasplots as aplt

def search(path,word="NTUP"):
    input = []
    for filename in os.listdir(path):
        fp = os.path.join(path,filename)
        if os.path.isfile(fp) and word in filename:
            input.append(fp)
    return input

aplt.set_atlas_style()
color = [root.kOrange,root.kAzure,root.kRed,root.kBlue,root.kGreen,root.kYellow,root.kGray,root.kBlack]

hist = []
dict = {
    #'SM tt~':["/afs/cern.ch/work/c/caiyi/2HDM/Run/13TeV.A10000.00403_/DAOD_TRUTH1",401.168031051],
    #'S':["/afs/cern.ch/work/c/caiyi/2HDM_SI/Run/13TeV.A500.00400_/DAOD_TRUTH1",17.7250013717],
    'S + I':["/afs/cern.ch/work/c/caiyi/2HDM_SI/Run/13TeV.A500.00401_/DAOD_TRUTH1",0.778070060212]
}

for f in dict.keys():
    fig1, ax1 = aplt.subplots(name="fig1", figsize=(800, 600))
    fig2, ax2 = aplt.subplots(name="fig2", figsize=(800, 600))
    fig3, ax3 = aplt.subplots(name="fig3", figsize=(800, 600))
    fig4, ax4 = aplt.subplots(name="fig4", figsize=(800, 600))
    fig5, ax5 = aplt.subplots(name="fig5", figsize=(800, 600))
    chain = root.TChain("CollectionTree")
    input = search(path=dict[f][0])
    for p in input:
        chain.Add(p)
    n = chain.GetEntries()
    n_h = len(hist)
    hist.append(root.TH1D("{}_epT".format(n_h),'',100,0,200))
    hist.append(root.TH1D("{}_eeta".format(n_h),'',100,-10,10))
    hist.append(root.TH1D("{}_mupT".format(n_h),'',100,0,200))
    hist.append(root.TH1D("{}_mueta".format(n_h),'',100,-10,10))
    hist.append(root.TH1D("{}_atw".format(n_h),'',60,0,120))
    hist.append(root.TH1D("{}_ptjet".format(n_h),'',100,0,200))
    hist.append(root.TH1D("{}_ptjetb".format(n_h),'',100,0,200))
    hist.append(root.TH1D("{}_etajet".format(n_h),'',100,-10,10))
    hist.append(root.TH1D("{}_etajetb".format(n_h),'',100,-10,10))
    for i in range(0,n):
        chain.GetEntry(i)
        n_l=0
        nx_l=0
        if chain.t_hasElectron[0]==1:
            n_l=n_l+1
        if chain.tx_hasElectron[0]==1:
            nx_l=nx_l+1
        if chain.t_hasMuon[0]==1:
            n_l=n_l+1
        if chain.tx_hasMuon[0]==1:
            nx_l=nx_l+1
        if n_l+nx_l==1:
            if chain.t_hasElectron[0]==1:
                pt_l=chain.t_e[0]
                pt_nu=chain.t_enu[0]
                hist[0].Fill(chain.t_e[0].Perp())
                hist[1].Fill(chain.t_e[0].Eta())
                hist[6].Fill(chain.t_b[0].Perp())
                hist[6].Fill(chain.tx_b[0].Perp())
                hist[5].Fill(chain.tx_j[0].Perp())
                hist[5].Fill(chain.tx_jx[0].Perp())
                hist[8].Fill(chain.t_b[0].Eta())
                hist[8].Fill(chain.tx_b[0].Eta())
                hist[7].Fill(chain.tx_j[0].Eta())
                hist[7].Fill(chain.tx_jx[0].Eta())
            elif chain.tx_hasElectron[0]==1:
                pt_l=chain.tx_e[0]
                pt_nu=chain.tx_enu[0]
                hist[0].Fill(chain.tx_e[0].Perp())
                hist[1].Fill(chain.tx_e[0].Eta())
                hist[6].Fill(chain.t_b[0].Perp())
                hist[6].Fill(chain.tx_b[0].Perp())
                hist[5].Fill(chain.t_j[0].Perp())
                hist[5].Fill(chain.t_jx[0].Perp())
                hist[8].Fill(chain.t_b[0].Eta())
                hist[8].Fill(chain.tx_b[0].Eta())
                hist[7].Fill(chain.t_j[0].Eta())
                hist[7].Fill(chain.t_jx[0].Eta())
            elif chain.t_hasMuon[0]==1:
                pt_l=chain.t_mu[0]
                pt_nu=chain.t_munu[0]
                hist[2].Fill(chain.t_mu[0].Perp())
                hist[3].Fill(chain.t_mu[0].Eta())
                hist[6].Fill(chain.t_b[0].Perp())
                hist[6].Fill(chain.tx_b[0].Perp())
                hist[5].Fill(chain.tx_j[0].Perp())
                hist[5].Fill(chain.tx_jx[0].Perp())
                hist[8].Fill(chain.t_b[0].Eta())
                hist[8].Fill(chain.tx_b[0].Eta())
                hist[7].Fill(chain.tx_j[0].Eta())
                hist[7].Fill(chain.tx_jx[0].Eta())
            elif chain.tx_hasMuon[0]==1:
                pt_l=chain.tx_mu[0]
                pt_nu=chain.tx_munu[0]
                hist[2].Fill(chain.tx_mu[0].Perp())
                hist[3].Fill(chain.tx_mu[0].Eta())
                hist[6].Fill(chain.t_b[0].Perp())
                hist[6].Fill(chain.tx_b[0].Perp())
                hist[5].Fill(chain.t_j[0].Perp())
                hist[5].Fill(chain.t_jx[0].Perp())
                hist[8].Fill(chain.t_b[0].Eta())
                hist[8].Fill(chain.tx_b[0].Eta())
                hist[7].Fill(chain.t_j[0].Eta())
                hist[7].Fill(chain.t_jx[0].Eta())
            ptl=root.TVector2(pt_l.X(),pt_l.Y())
            ptnu=root.TVector2(pt_nu.X(),pt_nu.Y())
            mTW=np.sqrt(2*pt_l.Perp()*pt_nu.Perp()*(1-np.cos(ptl.DeltaPhi(ptnu))))
            hist[4].Fill(mTW)

    ax1.plot(hist[0],label = "e", linecolor = color[0]+1,labelfmt = "L")
    ax1.plot(hist[2],label = "#mu", linecolor = color[1]+1,labelfmt = "L")
    ax1.add_margins(top=0.1)
    ax1.set_xlabel("p_{T} [GeV]")
    ax1.set_ylabel("Events / 2GeV")
    line = root.TLine(ax1.get_xlim()[0], 0, ax1.get_xlim()[1], 0)
    ax1.plot(line)
    ax1.legend(loc=(0.65, 0.78, 0.8, 0.90))
    fig1.savefig("~/eos/ttbar/1l_pt.png")

    ax2.plot(hist[1],label = "e", linecolor = color[0]+1,labelfmt = "L")
    ax2.plot(hist[3],label = "#mu", linecolor = color[1]+1,labelfmt = "L")
    ax2.add_margins(top=0.1)
    ax2.set_xlabel("#eta")
    ax2.set_ylabel("Events / 0.2")
    line = root.TLine(ax2.get_xlim()[0], 0, ax2.get_xlim()[1], 0)
    ax2.plot(line)
    ax2.legend(loc=(0.65, 0.78, 0.8, 0.90))
    fig2.savefig("~/eos/ttbar/1l_eta.png")

    ax3.plot(hist[4], linecolor = color[1]+1,labelfmt = "L")
    ax3.add_margins(top=0.1)
    ax3.set_xlabel("m_{T}^{W} [GeV]")
    ax3.set_ylabel("Events / 2 GeV")
    line = root.TLine(ax3.get_xlim()[0], 0, ax3.get_xlim()[1], 0)
    ax3.plot(line)
    ax3.legend(loc=(0.65, 0.78, 0.8, 0.90))
    fig3.savefig("~/eos/ttbar/1l_mtw.png")

    ax4.plot(hist[5],label = "Jet", linecolor = color[0]+1,labelfmt = "L")
    ax4.plot(hist[6],label = "Jet(b)", linecolor = color[1]+1,labelfmt = "L")
    ax4.add_margins(top=0.1)
    ax4.set_xlabel("p_{T} [GeV]")
    ax4.set_ylabel("Events / 2GeV")
    line = root.TLine(ax4.get_xlim()[0], 0, ax4.get_xlim()[1], 0)
    ax4.plot(line)
    ax4.legend(loc=(0.65, 0.78, 0.8, 0.90))
    fig4.savefig("~/eos/ttbar/1l_jetpt.png")

    ax5.plot(hist[7],label = "Jet", linecolor = color[0]+1,labelfmt = "L")
    ax5.plot(hist[8],label = "Jet(b)", linecolor = color[1]+1,labelfmt = "L")
    ax5.add_margins(top=0.1)
    ax5.set_xlabel("#eta")
    ax5.set_ylabel("Events / 0.2")
    line = root.TLine(ax5.get_xlim()[0], 0, ax5.get_xlim()[1], 0)
    ax5.plot(line)
    ax5.legend(loc=(0.65, 0.78, 0.8, 0.90))
    fig5.savefig("~/eos/ttbar/1l_jeteta.png")