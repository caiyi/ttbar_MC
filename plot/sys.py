#!/usr/bin/env python3
import os,re
import ROOT as root
import numpy as np
import atlasplots as aplt
import tarfile
import sys

aplt.set_atlas_style()
color = [root.kRed,root.kAzure,root.kGreen,root.kOrange,root.kYellow,root.kGray]
hist0 = []
hist1 = []
ratio_hist1 = []
ratio_ratio_hist1 = []
hist2 = []
ratio_hist2 = []
ratio_ratio_hist2 = []
hist3 = []
ratio_hist3 = []
ratio_ratio_hist3 = []
dict = {'500A0.4S Generated':'/afs/cern.ch/work/c/caiyi/2HDM_SI/Run/13TeV.A500.00400_/LHE/A500.00400_._00001.NTUP_LHE.root',
        '500A2.0S':'/afs/cern.ch/work/c/caiyi/2HDM_SI/Run/13TeV.A500.00400_/LHE/A500.00400_._00002.NTUP_LHE.root',
        '800A0.4S':'/afs/cern.ch/work/c/caiyi/2HDM_SI/Run/13TeV.A500.00400_/LHE/A500.00400_._00003.NTUP_LHE.root',
        '800A0.4S Generated':'/afs/cern.ch/work/c/caiyi/2HDM_SI/Run/13TeV.A500.00400_/LHE/A500.00400_._00004.NTUP_LHE.root',
        '500A0.4SI':'/afs/cern.ch/work/c/caiyi/2HDM_SI/Run/13TeV.A500.00400_/LHE/A500.00400_._00005.NTUP_LHE.root',
        }
fig1, (ax11, ax12) = aplt.ratio_plot(name="fig1", figsize=(800, 800), hspace=0.05)
fig2, (ax21, ax22) = aplt.ratio_plot(name="fig2", figsize=(800, 800), hspace=0.05)
fig3, (ax31, ax32) = aplt.ratio_plot(name="fig3", figsize=(800, 800), hspace=0.05)
fig, ax = aplt.subplots(name="fig", figsize=(800, 600))

for f in dict.keys():
    chain = root.TChain("CollectionTree")
    chain.Add(dict[f])
    n_h = len(hist0)
    print(n_h)

    hist0.append(root.TH1D("{}".format(n_h),'',60,300,900))
    hist1.append(root.TH1D("{}_1".format(n_h),'',60,300,900))
    hist2.append(root.TH1D("{}_2".format(n_h),'',60,300,900))
    hist3.append(root.TH1D("{}_3".format(n_h),'',60,300,900))
    n = chain.GetEntries()
    for i in range(0,n):
        chain.GetEntry(i)
        hist0[-1].Fill((chain.t[0]+chain.tx[0]).M(),chain.weight[0])
        hist1[-1].Fill((chain.t[0]+chain.tx[0]).M(),chain.weight[10])
        hist2[-1].Fill((chain.t[0]+chain.tx[0]).M(),chain.weight[235])
        hist3[-1].Fill((chain.t[0]+chain.tx[0]).M(),chain.weight[64])
    hist0[-1].Scale(139000/n)
    ax.plot(hist0[-1], label = f, linecolor = color[n_h]+1, labelfmt = "L")
    hist1[-1].Scale(139000/n)
    ratio_hist1.append(hist1[-1].Clone("ratio_hist1_{}".format(n_h)))
    ratio_hist1[-1].Divide(hist0[-1])
    ax11.plot(ratio_hist1[-1], label = f, linecolor = color[n_h]+1, labelfmt = "L")
    ratio_ratio_hist1.append(ratio_hist1[-1].Clone("ratio_ratio_hist1_{}".format(n_h)))
    ratio_ratio_hist1[-1].Divide(ratio_hist1[0])
    ax12.plot(ratio_ratio_hist1[-1], linecolor = color[n_h]+1, labelfmt = "L")
    hist2[-1].Scale(139000/n)
    ratio_hist2.append(hist2[-1].Clone("ratio_hist2_{}".format(n_h)))
    ratio_hist2[-1].Divide(hist0[-1])
    ax21.plot(ratio_hist2[-1], label = f, linecolor = color[n_h]+1, labelfmt = "L")
    ratio_ratio_hist2.append(ratio_hist2[-1].Clone("ratio_ratio_hist2_{}".format(n_h)))
    ratio_ratio_hist2[-1].Divide(ratio_hist2[0])
    ax22.plot(ratio_ratio_hist2[-1], linecolor = color[n_h]+1, labelfmt = "L")
    hist3[-1].Scale(139000/n)
    ratio_hist3.append(hist3[-1].Clone("ratio_hist3_{}".format(n_h)))
    ratio_hist3[-1].Divide(hist0[-1])
    ax31.plot(ratio_hist3[-1], label = f, linecolor = color[n_h]+1, labelfmt = "L")
    ratio_ratio_hist3.append(ratio_hist3[-1].Clone("ratio_ratio_hist3_{}".format(n_h)))
    ratio_ratio_hist3[-1].Divide(ratio_hist3[0])
    ax32.plot(ratio_ratio_hist3[-1], linecolor = color[n_h]+1, labelfmt = "L")

ax.cd()
line = root.TLine(ax.get_xlim()[0], 0, ax.get_xlim()[1], 0)
ax.plot(line)
ax.legend(loc=(0.65, 0.65, 0.85, 0.9))
ax.add_margins(top=0.1)
ax.set_xlabel("M_{t#bar{t}} [GeV]")
ax.set_ylabel("Events / 10GeV")
fig.savefig("ttbar/sys_tmp.png")

ax11.cd()
line1 = root.TLine(ax11.get_xlim()[0], 1, ax11.get_xlim()[1], 1)
ax11.plot(line1)
ax11.set_ylim(0.9, 1.5)
ax11.legend(loc=(0.65, 0.65, 0.85, 0.9))
ax11.text(0.25, 0.75, "MUR=0.5 MUF=0.5 alpsfact=0.5", size=24, align=13)
ax11.add_margins(top=0.1)
ax11.set_ylabel("sys_scale/weight0")
ax12.cd()
ax12.set_ylim(0.999, 1.001)
ax12.set_xlabel("M_{t#bar{t}} [GeV]")
ax12.set_ylabel("Ratio")
fig1.savefig("ttbar/sys_scale.png")

ax21.cd()
line2 = root.TLine(ax21.get_xlim()[0], 1, ax21.get_xlim()[1], 1)
ax21.plot(line2)
ax21.set_ylim(0.9, 1.5)
ax21.legend(loc=(0.65, 0.65, 0.85, 0.9))
ax21.text(0.25, 0.75, "PDF=260000 MemberID=100", size=24, align=13)
ax21.add_margins(top=0.1)
ax21.set_ylabel("sys_PDF/weight0")
ax22.cd()
ax22.set_ylim(0.999, 1.001)
ax22.set_xlabel("M_{t#bar{t}} [GeV]")
ax22.set_ylabel("Ratio")
fig2.savefig("ttbar/sys_pdf.png")

ax31.cd()
line3 = root.TLine(ax31.get_xlim()[0], 1, ax31.get_xlim()[1], 1)
ax31.plot(line3)
ax31.set_ylim(0.5, 1.5)
ax31.legend(loc=(0.65, 0.65, 0.85, 0.9))
ax31.text(0.25, 0.75, "dyn_scale_choice=sum pt", size=24, align=13)
ax31.add_margins(top=0.1)
ax31.set_ylabel("sys_dyn/weight0")
ax32.cd()
ax32.set_ylim(0.999, 1.001)
ax32.set_xlabel("M_{t#bar{t}} [GeV]")
ax32.set_ylabel("Ratio")
fig3.savefig("ttbar/sys_dyn.png")