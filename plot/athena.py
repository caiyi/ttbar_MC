#!/usr/bin/env python3
import os,re
import ROOT as root
import numpy as np
import atlasplots as aplt
import tarfile
import sys

def search(path,word="NTUP"):
    input = []
    for filename in os.listdir(path):
        fp = os.path.join(path,filename)
        if os.path.isfile(fp) and word in filename:
            input.append(fp)
    return input

aplt.set_atlas_style()
color = [root.kRed,root.kAzure,root.kOrange,root.kBlue,root.kGreen,root.kYellow,root.kGray,root.kBlack]
#fig1, (ax1, ax2) = aplt.ratio_plot(name="fig1", figsize=(800, 800), hspace=0.05)
fig1, ax1 = aplt.subplots(name="fig1", figsize=(800, 600))

ii = int(sys.argv[1])

hist = []
dict = {
    '400 2HDM S Pythia6':["2HDM_MS1/Run/13TeV.A400.00400_",1],
    '400 2HDM S Pythia8':["2HDM_MS/Run/13TeV.A400.00400_",2],
    '700 2HDM S Pythia6':["2HDM_MS1/Run/13TeV.A700.00400_bak",3]
}

ax1.cd()
if (ii == 1):
    for f in dict.keys():
        if not(dict[f][1] in [1,2]):
            continue
        tarpath = search(path=os.path.join('/afs/cern.ch/work/c/caiyi',dict[f][0],'LHE'),word='001.tar.gz')
        lhepath = os.path.join('/tmp/cyz',f)
        tar = tarfile.open(tarpath[0])
        tar.extractall(path=lhepath)
        lines = open(os.path.join(lhepath,tar.getnames()[0]),'r').readlines()
        for line in lines:
            if 'Integrated weight (pb)' in line:
                break
        xs=float(re.findall(r"\d+\.?\d*",line)[0])
        print(xs)
        chain = root.TChain("CollectionTree")
        input = search(path=os.path.join('/afs/cern.ch/work/c/caiyi',dict[f][0],'DAOD_TRUTH1'))
        for p in input:
            chain.Add(p)
        n_h = len(hist)
        hist.append(root.TH1D("{}".format(n_h),'',70,300,1000))
        n = chain.GetEntries()
        for i in range(0,n):
            chain.GetEntry(i)
            hist[-1].Fill(chain.ttx[0].M(),chain.weight[0])
        total_weight = hist[-1].Integral()
        print(total_weight)
        hist[-1].Scale(139000*xs/total_weight)
        ax1.plot(hist[-1],label = f, linecolor = color[n_h]+1,labelfmt = "L")

    line1 = root.TLine(ax1.get_xlim()[0], 0, ax1.get_xlim()[1], 0)
    ax1.plot(line1)
    ax1.legend(loc=(0.45, 0.58, 0.8, 0.90))
    ax1.add_margins(top=0.1)
    ax1.set_xlabel("M_{t#bar{t}} [GeV]")
    ax1.set_ylabel("Events / 10GeV")
    fig1.savefig("ttbar/ath1.png")

if (ii == 2):
    for f in dict.keys():
        if not(dict[f][1] in [1]):
            continue
    tarpath = search(path=os.path.join('/afs/cern.ch/work/c/caiyi',dict[f][0],'LHE'),word='001.tar.gz')
    lhepath = os.path.join('/tmp/cyz',f)
    tar = tarfile.open(tarpath[0])
    tar.extractall(path=lhepath)
    lines = open(os.path.join(lhepath,tar.getnames()[0]),'r').readlines()
    for line in lines:
        if 'Integrated weight (pb)' in line:
            break
    xs=float(re.findall(r"\d+\.?\d*",line)[0])
    chain = root.TChain("CollectionTree")
    input = search(path=os.path.join('/afs/cern.ch/work/c/caiyi',dict[f][0],'DAOD_TRUTH1'))
    for p in input:
        chain.Add(p)
    n_h = len(hist)
    hist.append(root.TH1D("{}_cos".format(n_h),'',20,-1,1))
    n = chain.GetEntries()
    for i in range(0,n):
        chain.GetEntry(i)
        n_l=0
        nx_l=0
        if chain.t_hasElectron[0]==1:
            n_l=n_l+1
        if chain.tx_hasElectron[0]==1:
            nx_l=nx_l+1
        if chain.t_hasMuon[0]==1:
            n_l=n_l+1
        if chain.tx_hasMuon[0]==1:
            nx_l=nx_l+1
        if n_l+nx_l==2:
            if chain.t_hasElectron[0]==1:
                t_l=chain.t_e[0]
                t_nu=chain.t_enu[0]
            elif chain.t_hasMuon[0]==1:
                t_l=chain.t_mu[0]
                t_nu=chain.t_munu[0]
            if chain.tx_hasElectron[0]==1:
                tx_l=chain.tx_e[0]
                tx_nu=chain.tx_enu[0]
            elif chain.tx_hasMuon[0]==1:
                tx_l=chain.tx_mu[0]
                tx_nu=chain.tx_munu[0]
            t_l.Boost(-chain.ttx[0].BoostVector())
            chain.t[0].Boost(-chain.ttx[0].BoostVector())
            t_l.Boost(-chain.t[0].BoostVector())
            tx_l.Boost(-chain.ttx[0].BoostVector())
            chain.tx[0].Boost(-chain.ttx[0].BoostVector())
            tx_l.Boost(-chain.tx[0].BoostVector())
            hist[-1].Fill(np.cos(t_l.Angle(tx_l.Vect())),chain.weight[0])
    hist[-1].Scale(139000*xs/hist[-1].Integral())
    ax1.plot(hist[-1],label = f, linecolor = color[n_h]+1,labelfmt = "L")

    line1 = root.TLine(ax1.get_xlim()[0], 0, ax1.get_xlim()[1], 0)
    ax1.plot(line1)
    ax1.legend(loc=(0.45, 0.58, 0.8, 0.90))
    ax1.add_margins(top=0.1)
    ax1.set_xlabel("c_{hel}")
    ax1.set_ylabel("Events / 0.1")
    fig1.savefig("ttbar/ath2.png")

if (ii == 3):
    for f in dict.keys():
        if not(dict[f][1] in [3]):
            continue
        tarpath = search(path=os.path.join('/afs/cern.ch/work/c/caiyi',dict[f][0],'LHE'),word='001.tar.gz')
        lhepath = os.path.join('/tmp/cyz',f)
        tar = tarfile.open(tarpath[0])
        tar.extractall(path=lhepath)
        lines = open(os.path.join(lhepath,tar.getnames()[0]),'r').readlines()
        for line in lines:
            if 'Integrated weight (pb)' in line:
                break
        xs=float(re.findall(r"\d+\.?\d*",line)[0])
        chain = root.TChain("CollectionTree")
        input = search(path=os.path.join('/afs/cern.ch/work/c/caiyi',dict[f][0],'DAOD_TRUTH1'))
        for p in input:
            chain.Add(p)
        n_h = len(hist)
        hist.append(root.TH1D("{}".format(n_h),'',80,0,800))
        n = chain.GetEntries()
        for i in range(0,n):
            chain.GetEntry(i)
            if (chain.t[0].Pt()>chain.tx[0].Pt()):
                hist[-1].Fill(chain.t[0].Pt(),chain.weight[0])
            else:
                hist[-1].Fill(chain.tx[0].Pt(),chain.weight[0])
        hist[-1].Scale(139000*xs/hist[-1].Integral())
        ax1.plot(hist[-1],label = f, linecolor = color[n_h]+1,labelfmt = "L")

    line1 = root.TLine(ax1.get_xlim()[0], 0, ax1.get_xlim()[1], 0)
    ax1.plot(line1)
    ax1.legend(loc=(0.45, 0.58, 0.8, 0.90))
    ax1.add_margins(top=0.1)
    ax1.set_xlabel("Pt of Leading Top [GeV]")
    ax1.set_ylabel("Events / 10GeV")
    fig1.savefig("ttbar/ath4.png")