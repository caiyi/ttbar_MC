#!/usr/bin/env python3
import os
import ROOT as root
import numpy as np
import atlasplots as aplt

def search(path,word="NTUP"):
    input = []
    for filename in os.listdir(path):
        fp = os.path.join(path,filename)
        if os.path.isfile(fp) and word in filename:
            input.append(fp)
    return input

aplt.set_atlas_style()
color = [root.kRed,root.kOrange,root.kBlue,root.kAzure,root.kGreen,root.kYellow,root.kGray,root.kBlack]
fig, (ax1, ax2) = aplt.ratio_plot(name="fig1", figsize=(800, 800), hspace=0.05)

hist = []
dict = {
    'SM tt~':["/afs/cern.ch/work/c/caiyi/2HDM/Run/13TeV.A10000.00403_/DAOD_TRUTH1",401.168031051],
    'SM tt~ + SI(A->tt~) width=5%mass':["/afs/cern.ch/work/c/caiyi/2HDM/Run/13TeV.A500.00402_/DAOD_TRUTH1",520.651040291],
    'SM tt~ + SI(A->tt~) width=143.907GeV':["/afs/cern.ch/work/c/caiyi/2HDM_SI/Run/13TeV.A500.00402_/DAOD_TRUTH1",401.914331103],
    #'SM tt~ + SI(A->tt~) width=143.907GeV':["/afs/cern.ch/work/c/caiyi/2HDM_SI/Run/13TeV.A500.00402_/LHE",401.914331103]
}

for f in dict.keys():
    chain = root.TChain("CollectionTree")
    input = search(path=dict[f][0])
    for p in input:
        chain.Add(p)
    n = chain.GetEntries()
    n_h = len(hist)
    hist.append(root.TH1D("{}".format(n_h),'',20,400,600))
    for i in range(0,n):
        chain.GetEntry(i)
        hist[-1].Fill(chain.ttx[0].M(),chain.weight[0])
    hist[-1].Scale(139000*dict[f][1]/hist[-1].Integral())
    ax1.plot(hist[-1],linecolor = color[n_h]+1,label = f,labelfmt = "L")

line = root.TLine(ax1.get_xlim()[0], 1, ax1.get_xlim()[1], 1)
ax2.plot(line)
ratio_hist1 = hist[1].Clone("ratio_hist")
ratio_hist1.Divide(hist[0])
ax2.plot(ratio_hist1, linecolor=color[1]+1)
ratio_hist2 = hist[2].Clone("ratio_hist")
ratio_hist2.Divide(hist[0])
ax2.plot(ratio_hist2, linecolor=color[2]+1)

ax1.add_margins(top=0.1)
ax2.add_margins(top=0.05)
ax2.set_xlabel("M_t#bar{t} [GeV]")
ax1.set_ylabel("Events / 10GeV")
ax2.set_ylabel("S+I+B / SM tt~")
ax1.cd()
ax1.legend(loc=(0.58, 0.78, 0.8, 0.90),textsize=16)
fig.savefig("~/eos/ttbar/SM_SIB.png")