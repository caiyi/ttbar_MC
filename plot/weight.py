#!/usr/bin/env python3
import os,re
import ROOT as root
import numpy as np
import atlasplots as aplt
import tarfile
import sys

def search(path,word="NTUP"):
    input = []
    for filename in os.listdir(path):
        fp = os.path.join(path,filename)
        if os.path.isfile(fp) and word in filename:
            input.append(fp)
    return input

aplt.set_atlas_style()
color = [root.kRed,root.kAzure,root.kOrange,root.kBlue,root.kGreen,root.kYellow,root.kGray,root.kBlack]
fig1, ax1 = aplt.subplots(name="fig1", figsize=(800, 600))

ii = int(sys.argv[1])

hist = []
dict = {
    '500SI 2HDM iseed=1':["2HDM_MS/Run_0/13TeV.A500.00401_",0],
    '500SI 2HDMa iseed=1':["2HDM_MS_all/Run_0/13TeV.A500.00401_100.0000",1],
    '500SI 2HDM iseed=11':["2HDM_MS/Run_1/13TeV.A500.00401_",2],
    '500SI 2HDMa iseed=11':["2HDM_MS_all/Run_1/13TeV.A500.00401_100.0000",3],
    '500SI 2HDM iseed=21':["2HDM_MS/Run_2/13TeV.A500.00401_",4],
    '500SI 2HDMa iseed=21':["2HDM_MS_all/Run_2/13TeV.A500.00401_100.0000",5],
    '500SI 2HDM iseed=31':["2HDM_MS/Run_3/13TeV.A500.00401_",6],
    '500SI 2HDMa iseed=31':["2HDM_MS_all/Run_3/13TeV.A500.00401_100.0000",7],
    '500SI 2HDM iseed=41':["2HDM_MS/Run_4/13TeV.A500.00401_",8],
    '500SI 2HDMa iseed=41':["2HDM_MS_all/Run_4/13TeV.A500.00401_100.0000",9],
    '500SI 2HDM iseed=51':["2HDM_MS/Run_5/13TeV.A500.00401_",10],
    '500SI 2HDMa iseed=51':["2HDM_MS_all/Run_5/13TeV.A500.00401_100.0000",11],
}

ax1.cd()
for f in dict.keys():
    #if not(dict[f][1] in [10+2*ii,11+2*ii]):
    if not(dict[f][1] in [0+ii,2+ii,4+ii,6+ii,8+ii,10+ii]):
        continue
    chain = root.TChain("CollectionTree")
    input = search(path=os.path.join('/afs/cern.ch/work/c/caiyi',dict[f][0],'DAOD_TRUTH1'))
    for p in input:
        chain.Add(p)
    n_h = len(hist)
    hist.append(root.TH1D("{}".format(n_h),'',6,-3e-08,3e-08))
    n = chain.GetEntries()
    chain.GetEntry(1)
    print(chain.weight[0])
    for i in range(0,n):
        chain.GetEntry(i)
        n_l=0
        nx_l=0
        hist[-1].Fill(chain.weight[0])
    ax1.plot(hist[-1],label = f, linecolor = color[n_h]+1,labelfmt = "L")

line1 = root.TLine(ax1.get_xlim()[0], 0, ax1.get_xlim()[1], 0)
ax1.plot(line1)
ax1.legend(loc=(0.35, 0.58, 0.65, 0.90))
ax1.add_margins(top=0.1)
ax1.set_xlabel("Weight")
fig1.savefig("ttbar/weight{}.png".format(ii))