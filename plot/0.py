#!/usr/bin/env python3
import os
import ROOT as root
import numpy as np
import atlasplots as aplt

def search(path,word="NTUP"):
    input = []
    for filename in os.listdir(path):
        fp = os.path.join(path,filename)
        if os.path.isfile(fp) and word in filename:
            input.append(fp)
    return input

aplt.set_atlas_style()
color = [root.kRed,root.kAzure,root.kOrange,root.kBlue,root.kGreen,root.kYellow,root.kGray,root.kBlack]
fig, ax = aplt.subplots(name="fig1", figsize=(800, 600))

hist = []
dict = {
    'SM t#bar{t}':["/afs/cern.ch/work/c/caiyi/2HDM_SI/Run/13TeV.A500.00403_/DAOD_TRUTH1",401.132031042],
    #'S':["/afs/cern.ch/work/c/caiyi/2HDM_SI/Run/13TeV.A500.00400_/DAOD_TRUTH1",17.7250013717],
    'S + I':["/afs/cern.ch/work/c/caiyi/2HDM_SI/Run/13TeV.A500.00401_/DAOD_TRUTH1",0.778070060212]
}

for f in dict.keys():
    chain = root.TChain("CollectionTree")
    input = search(path=dict[f][0])
    for p in input:
        chain.Add(p)
    n = chain.GetEntries()
    n_h = len(hist)
    hh=root.TH1D("t_{}".format(n_h),'',70,300,1000)
    hist.append(root.TH1D("{}".format(n_h),'',70,300,1000))
    for i in range(0,n):
        chain.GetEntry(i)
        n_l=0
        nx_l=0
        hh.Fill(chain.ttx[0].M(),chain.weight[0])
        hist[-1].Fill(chain.ttx[0].M(),chain.weight[0])
    hist[-1].Scale(139000*dict[f][1]/hh.Integral())
    ax.plot(hist[-1],label = f, linecolor = color[n_h]+1,labelfmt = "L")

ax.add_margins(top=0.1)
ax.set_xlabel("M_{t#bar{t}} [GeV]")
ax.set_ylabel("Events / 10GeV")
ax.legend(loc=(0.58, 0.78, 0.8, 0.90))
line = root.TLine(ax.get_xlim()[0], 0, ax.get_xlim()[1], 0)
ax.plot(line)
fig.savefig("~/eos/ttbar/0.png")