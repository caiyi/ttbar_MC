import utils
import ROOT
import os

utils.rcSetup(os.path.join("Core", "AnalysisTop"))
chain = ROOT.TChain("CollectionTree")
chain.Add("/afs/cern.ch/user/c/caiyi/work/2HDM_MS/Run/13TeV.A400.00400_/DAOD_TRUTH1/A400.00400_._00004.DAOD_TRUTH.root")
tree = ROOT.xAOD.MakeTransientTree(chain)
tree.GetEntry(1)
for p in tree.TruthParticles:
    print(p.pdgId(),p.status(),p.child().pdgId())

for t in tree.TruthEvents:
    print(t.pdfInfo().x1)

