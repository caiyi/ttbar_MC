#!/usr/bin/env python3
import os
import ROOT as root
import numpy as np
import atlasplots as aplt

def search(path,word="NTUP"):
    input = []
    for filename in os.listdir(path):
        fp = os.path.join(path,filename)
        if os.path.isfile(fp) and word in filename:
            input.append(fp)
    return input

aplt.set_atlas_style()
color = [root.kRed,root.kAzure,root.kOrange,root.kBlue,root.kGreen,root.kYellow,root.kGray,root.kBlack]
fig, ax = aplt.subplots(name="fig1", figsize=(800, 600))
fig2, ax2 = aplt.subplots(name="fig2", figsize=(800, 600))

hist = []
dict = {
    'SM t#bar{t}':["/afs/cern.ch/work/c/caiyi/2HDM/Run/13TeV.A10000.00403_/DAOD_TRUTH1",401.132031042],
    #'S':["/afs/cern.ch/work/c/caiyi/2HDM_SI/Run/13TeV.A500.00400_/DAOD_TRUTH1",17.7250013717],
    '(S + I) * 20':["/afs/cern.ch/work/c/caiyi/2HDM_SI/Run/13TeV.A500.00401_/DAOD_TRUTH1",20*0.778070060212]
}

for f in dict.keys():
    chain = root.TChain("CollectionTree")
    input = search(path=dict[f][0])
    for p in input:
        chain.Add(p)
    n = chain.GetEntries()
    n_h = len(hist)
    hh=root.TH1D("t_{}".format(n_h),'',70,300,1000)
    hist.append(root.TH1D("{}_mtt".format(n_h),'',70,300,1000))
    hist.append(root.TH1D("{}_cos".format(n_h),'',50,0,1))
    for i in range(0,n):
        chain.GetEntry(i)
        n_l=0
        nx_l=0
        hh.Fill(chain.ttx[0].M(),chain.weight[0])
        if chain.t_b[0].Perp()>20 and chain.tx_b[0].Perp()>20 and  np.abs(chain.t_b[0].Eta())<2.4 and np.abs(chain.tx_b[0].Eta())<2.4:
            if chain.t_hasElectron[0]==1 and chain.t_e[0].Perp()>30 and np.abs(chain.t_e[0].Eta())<2.5 and chain.tx_j[0].Perp()>20 and chain.tx_jx[0].Perp()>20 and  np.abs(chain.tx_j[0].Eta())<2.4 and np.abs(chain.tx_jx[0].Eta())<2.4:
                n_l=n_l+1
            if chain.tx_hasElectron[0]==1 and chain.tx_e[0].Perp()>30 and np.abs(chain.tx_e[0].Eta())<2.5 and chain.t_j[0].Perp()>20 and chain.t_jx[0].Perp()>20 and  np.abs(chain.t_j[0].Eta())<2.4 and np.abs(chain.t_jx[0].Eta())<2.4:
                nx_l=nx_l+1
            if chain.t_hasMuon[0]==1 and chain.t_mu[0].Perp()>26 and np.abs(chain.t_mu[0].Eta())<2.4 and chain.tx_j[0].Perp()>20 and chain.tx_jx[0].Perp()>20 and  np.abs(chain.tx_j[0].Eta())<2.4 and np.abs(chain.tx_jx[0].Eta())<2.4:
                n_l=n_l+1
            if chain.tx_hasMuon[0]==1 and chain.tx_mu[0].Perp()>26 and np.abs(chain.tx_mu[0].Eta())<2.4 and chain.t_j[0].Perp()>20 and chain.t_jx[0].Perp()>20 and  np.abs(chain.t_j[0].Eta())<2.4 and np.abs(chain.t_jx[0].Eta())<2.4:
                nx_l=nx_l+1
            if n_l+nx_l==1:
                if chain.t_hasElectron[0]==1:
                    p_l=chain.t_e[0]
                    p_nu=chain.t_enu[0]
                    p_t=chain.t[0]
                    p_t.Boost(-chain.ttx[0].BoostVector())
                elif chain.tx_hasElectron[0]==1:
                    p_l=chain.tx_e[0]
                    p_nu=chain.tx_enu[0]
                    p_t=chain.tx[0]
                    p_t.Boost(-chain.ttx[0].BoostVector())
                elif chain.t_hasMuon[0]==1:
                    p_l=chain.t_mu[0]
                    p_nu=chain.t_munu[0]
                    p_t=chain.t[0]
                    p_t.Boost(-chain.ttx[0].BoostVector())
                elif chain.tx_hasMuon[0]==1:
                    p_l=chain.tx_mu[0]
                    p_nu=chain.tx_munu[0]
                    p_t=chain.tx[0]
                    chain.ttx[0].Boost(-p_t.Vect())
                    p_t.Boost(-chain.ttx[0].BoostVector())
                ptl=root.TVector2(p_l.X(),p_l.Y())
                ptnu=root.TVector2(p_nu.X(),p_nu.Y())
                mTW=np.sqrt(2*p_l.Perp()*p_nu.Perp()*(1-np.cos(ptl.DeltaPhi(ptnu))))
                if mTW>50:
                    hist[n_h].Fill(chain.ttx[0].M(),chain.weight[0])
                    hist[n_h+1].Fill(np.abs(np.cos(p_t.Angle(chain.ttx[0].Vect()))),chain.weight[0])
    hist[n_h].Scale(139000*dict[f][1]/hh.Integral())
    hist[n_h+1].Scale(139000*dict[f][1]/hh.Integral())
    ax.plot(hist[n_h],label = f, linecolor = color[n_h]+1,labelfmt = "L")
    #ax2.plot(hist[n_h+1],label = f, linecolor = color[n_h]+1,labelfmt = "L")

ax.add_margins(top=0.1)
ax.set_xlabel("M_{t#bar{t}} [GeV]")
ax.set_ylabel("Events / 10GeV")
ax.legend(loc=(0.58, 0.78, 0.8, 0.90))
line = root.TLine(ax.get_xlim()[0], 0, ax.get_xlim()[1], 0)
ax.plot(line)
fig.savefig("~/eos/ttbar/1l_mtt.png")

ax2.add_margins(top=0.1)
ax2.set_xlabel("|cos#theta|")
ax2.set_ylabel("Events / 0.02")
ax2.legend(loc=(0.58, 0.78, 0.8, 0.90))
line2 = root.TLine(ax2.get_xlim()[0], 0, ax2.get_xlim()[1], 0)
ax2.plot(line2)
#fig2.savefig("~/eos/ttbar/1l_cos.png")

print(hist[0].GetEntries())
print(hist[2].GetEntries())