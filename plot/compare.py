#!/usr/bin/env python3
import os,re
import ROOT as root
import numpy as np
import atlasplots as aplt
import tarfile
import sys

def search(path,word="NTUP"):
    input = []
    for filename in os.listdir(path):
        fp = os.path.join(path,filename)
        if os.path.isfile(fp) and word in filename:
            input.append(fp)
    return input

aplt.set_atlas_style()
color = [root.kRed,root.kAzure,root.kOrange,root.kBlue,root.kGreen,root.kYellow,root.kGray,root.kBlack]
#fig1, (ax1, ax2) = aplt.ratio_plot(name="fig1", figsize=(800, 800), hspace=0.05)
fig1, ax1 = aplt.subplots(name="fig1", figsize=(800, 600))

ii = int(sys.argv[1])

hist = []
dict = {
    '500 S 2HDM MG':["2HDM_MG/Run/13TeV.A500.00400_",0],
    '500 S 2HDM MG_all':["2HDM_MG_all/Run/13TeV.A500.00400_",1],
    '500 S 2HDM0 MS_all':["2HDM_MS/Run/13TeV.A500.00400_",2],
    '500 SI 2HDM0 MS_all':["2HDM_MS/Run/13TeV.A500.00401_",3],
    '500 S 2HDM MS_all':["2HDM_MS_all/Run/13TeV.A500.00400_",4],
    '500 SI 2HDM MS_all':["2HDM_MS_all/Run/13TeV.A500.00401_",5],
    '500 S 2HDMa MS_all':["2HDM_MS_all/Run/13TeV.A500.00400_100.0000",6],
    '500 SI 2HDMa MS_all':["2HDM_MS_all/Run/13TeV.A500.00401_100.0000",7],
    '500 S+I+B 2HDM MS_all':["2HDM_MS_all/Run_bak/13TeV.A500.00402_",8],
    '500 S+I+B 2HDMa MS_all':["2HDM_MS_all/Run_bak/13TeV.A500.00402_100.0000",9],
    '500SI 2HDM iseed=1':["2HDM_MS/Run_0/13TeV.A500.00401_",10],
    '500SI 2HDMa iseed=1':["2HDM_MS_all/Run_0/13TeV.A500.00401_100.0000",11],
    '500SI 2HDM iseed=11':["2HDM_MS/Run_1/13TeV.A500.00401_",12],
    '500SI 2HDMa iseed=11':["2HDM_MS_all/Run_1/13TeV.A500.00401_100.0000",13],
    '500SI 2HDM iseed=21':["2HDM_MS/Run_2/13TeV.A500.00401_",14],
    '500SI 2HDMa iseed=21':["2HDM_MS_all/Run_2/13TeV.A500.00401_100.0000",15],
    '500SI 2HDM iseed=31':["2HDM_MS/Run_3/13TeV.A500.00401_",16],
    '500SI 2HDMa iseed=31':["2HDM_MS_all/Run_3/13TeV.A500.00401_100.0000",17],
    '500SI 2HDM iseed=41':["2HDM_MS/Run_4/13TeV.A500.00401_",18],
    '500SI 2HDMa iseed=41':["2HDM_MS_all/Run_4/13TeV.A500.00401_100.0000",19],
    '500SI 2HDM iseed=51':["2HDM_MS/Run_5/13TeV.A500.00401_",20],
    '500SI 2HDMa iseed=51':["2HDM_MS_all/Run_5/13TeV.A500.00401_100.0000",21],
    '500SI 2HDM iseed=1':["2HDM_MS_all/Run_h_0/13TeV.A500.00401_",22],
    '500SI 2HDMa iseed=1':["2HDM_MS/Run_h_0/13TeV.A500.00401_100.0000",23],
    '500SI 2HDM iseed=11':["2HDM_MS_all/Run_h_1/13TeV.A500.00401_",24],
    '500SI 2HDMa iseed=11':["2HDM_MS/Run_h_1/13TeV.A500.00401_100.0000",25],
    '500SI 2HDM iseed=21':["2HDM_MS_all/Run_h_2/13TeV.A500.00401_",26],
    '500SI 2HDMa iseed=21':["2HDM_MS/Run_h_2/13TeV.A500.00401_100.0000",27]
}

ax1.cd()
for f in dict.keys():
    #if not(dict[f][1] in [10+2*ii,11+2*ii]):
    if not(dict[f][1] in [22,23,24,25,26,27]):
        continue
    tarpath = search(path=os.path.join('/afs/cern.ch/work/c/caiyi',dict[f][0],'LHE'),word='001.tar.gz')
    lhepath = os.path.join('/tmp/cyz',f)
    tar = tarfile.open(tarpath[0])
    tar.extractall(path=lhepath)
    lines = open(os.path.join(lhepath,tar.getnames()[0]),'r').readlines()
    for line in lines:
        if 'Integrated weight (pb)' in line:
            break
    xs=float(re.findall(r"\d+\.?\d*",line)[0])
    print(xs)
    chain = root.TChain("CollectionTree")
    input = search(path=os.path.join('/afs/cern.ch/work/c/caiyi',dict[f][0],'DAOD_TRUTH1'))
    for p in input:
        chain.Add(p)
    n_h = len(hist)
    hist.append(root.TH1D("{}".format(n_h),'',70,300,1000))
    n = chain.GetEntries()
    for i in range(0,n):
        chain.GetEntry(i)
        n_l=0
        nx_l=0
        hist[-1].Fill(chain.ttx[0].M(),chain.weight[0])
    total_weight = hist[-1].Integral()
    print(total_weight)
    hist[-1].Scale(139000/total_weight)
    ax1.plot(hist[-1],label = f, linecolor = color[n_h]+1,labelfmt = "L")
'''

line1 = root.TLine(ax1.get_xlim()[0], 0, ax1.get_xlim()[1], 0)
ax1.plot(line1)
ax1.legend(loc=(0.45, 0.78, 0.8, 0.90))
line2 = root.TLine(ax1.get_xlim()[0], 1, ax1.get_xlim()[1], 1)
ax2.plot(line2)
ratio_hist1 = hist[1].Clone("ratio_hist")
ratio_hist1.Divide(hist[0])
ax2.plot(ratio_hist1, "P")
ax2.set_ylim(0.5,2)

ax1.add_margins(top=0.1)
ax2.add_margins(top=0.05)
ax2.set_xlabel("M_{t#bar{t}} [GeV]")
ax1.set_ylabel("Events / 10GeV")
ax2.set_ylabel("2HDMa / 2HDM")
fig1.savefig("ttbar/tmp_seed{}.png".format(ii))
'''

line1 = root.TLine(ax1.get_xlim()[0], 0, ax1.get_xlim()[1], 0)
ax1.plot(line1)
ax1.legend(loc=(0.45, 0.58, 0.8, 0.90))
ax1.add_margins(top=0.1)
ax1.set_xlabel("M_{t#bar{t}} [GeV]")
ax1.set_ylabel("Events / 10GeV")
fig1.savefig("ttbar/xs0hseed.png")
